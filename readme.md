For practicing SQLALCHEMY and following along with the documentation

Try the following

in cmd:
Python
>>>import models
>>>david = models.Friend(name="David", age=29, location="New York")
>>>models.session.add(david)
>>>models.session.new #to check that it was added correctly
>>>models.session.commit()
>>>models.session.new
>>>exit()

Now we have saved a friend named david to our db
Can launch sqlite3 db (if installed)
$sqlite3 friends.db
sqlite> .tables
sqlite> SELECT * FROM friends