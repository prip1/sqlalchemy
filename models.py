from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///friends.db", echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()


class Friend(Base):
    __tablename__ = 'friends'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)
    location = Column(String)

    def __repr__(self):
        return f'{self.id} | {self.name} {self.age} {self.location}'


if __name__ == '__main__':
    Base.metadata.create_all(engine)