#from . import db # the . means "from package" - the website package we just created. Want to grab db from init, which is a part of the package
from flask_login import UserMixin #going to inherit some properties for our user object
from sqlalchemy.sql import func
from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path

engine = create_engine("sqlite:///league.db", echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()
db = SQLAlchemy()
DB_NAME = "database.db"


user_summoner = db.Table('user_summoner',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('summoner_puuid', db.String(36), db.ForeignKey('summoner.puuid'))
)

summoner_game = db.Table('summoner_game',
    db.Column('summoner_puuid', db.String(36), db.ForeignKey('summoner.puuid')),
    db.Column('match_outcome', db.Integer),
    db.Column('match_id', db.String(14), db.ForeignKey('game.match_id'))
)

#User will inherit from db.Model and UserMixin
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True) 
    email = db.Column(db.String(150), unique=True) #string with maximum length of 150 and email has to be unique
    password = db.Column(db.String(150))
    summoners = db.relationship('Summoner', secondary=user_summoner, backref="users")

    def __repr__(self):
        return f'<User: {self.id}>'


class Summoner(db.Model):
    puuid = db.Column(db.String(36), primary_key=True)
    summoner_name = db.Column(db.String(100))
    summoner_level = db.Column(db.Integer)
    games = db.relationship('Game', secondary=summoner_game, backref="summoners")

class Game(db.Model):
    match_id = db.Column(db.String(40), primary_key=True)
    winning_team = db.Column(db.Integer) #save as 1 or 2 for: team 100 and team 200
    # champion_top1 = db.Column(db.String('25'))
    # champion_jungle1 = db.Column(db.String('25'))
    # champion_mid1 = db.Column(db.String('25'))
    # champion_adc1 = db.Column(db.String('25'))
    # champion_support1 = db.Column(db.String('25'))
    # champion_top2 = db.Column(db.String('25'))
    # champion_jungle2 = db.Column(db.String('25'))
    # champion_mid2 = db.Column(db.String('25'))
    # champion_adc2 = db.Column(db.String('25'))
    # champion_support2 = db.Column(db.String('25'))
    # summoner_name_top1 = db.Column(db.String('25'))
    # summoner_name_jungle1 = db.Column(db.String('25'))
    # summoner_name_mid1 = db.Column(db.String('25'))
    # summoner_name_adc1 = db.Column(db.String('25'))
    # summoner_name_support1 = db.Column(db.String('25'))
    # summoner_name_top2 = db.Column(db.String('25'))
    # summoner_name_jungle2 = db.Column(db.String('25'))
    # summoner_name_mid2 = db.Column(db.String('25'))
    # summoner_name_adc2 = db.Column(db.String('25'))
    # summoner_name_support2 = db.Column(db.String('25'))

def create_database(app):
    if not path.exists('website/' + DB_NAME):
        db.create_all(app=app)
        print('Created Database!')
    
if __name__ == '__main__':
    Base.metadata.create_all(engine)
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'asdfdrfg' #grab environment variable names 'SECRET_KEY'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}' #f character makes whatever is in {} to be evaluated as python string
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
    db.init_app(app)
    create_database(app)

#testing commit

